Made for UNCC 49er Game Jam

Sound Credits:

used in bullet sound (heavily edited){
	Remington_field_shot: fastson
	cannon: man
}

used in pain & death sound (not-so-heavily edited){
	Adolescent_PainScream01: Vosvoy
	splat_005: yottasounds
}

All available at freesounds.org under the Creative Commons License

Link to Creative Commons License:
http://creativecommons.org/licenses/by/3.0/